# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(string):
    new_string = ""
    for letter in string:
        if letter == "Z":
            new_letter = "A"
        elif letter == "z":
            new_letter = "a"
        else:
            new_letter = chr(ord(letter) + 1)
        new_string += new_letter
    return new_string


print(shift_letters("Kala"))


def shifted_letters(word):
    new_word = ""
    alphabet = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"
    for letter in word:
        if letter == "Z":
            new_letter = "A"
        elif letter == "z":
            new_letter = "a"
        else:
            for char in alphabet:
                if letter == char:
                    new_letter = alphabet[alphabet.index(char) + 2]
                    new_word += new_letter
    return new_word


print(shifted_letters("import"))
