# Write a class that meets these requirements.
#
# Name:       Book
#
# Required state:
#    * author name, a string
#    * title, a string
#
# Behavior:
#    * get_author: should return "Author: «author name»"
#    * get_title:  should return "Title: «title»"


class Book:
    def __init__(self, author, title) -> None:
        self.author = author
        self.title = title

    def get_author(self):
        return "Author: " + self.author

    def get_title(self):
        return "Title: " + self.title


book1 = Book("Natalie Zina Walschots", "Hench")
print(book1.get_author())
print(book1.get_title())
