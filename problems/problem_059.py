# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one of the numbers
import random


def specific_random():
    numbers = []
    for i in range(10, 501):
        if i % 35 == 0:
            numbers.append(i)
            num = random.choice(numbers)
    return (num)


print(specific_random())
