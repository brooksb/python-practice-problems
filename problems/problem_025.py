# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# #calculate_sum function
# If the list of values is empty, the function should
#  return None
# sum = 0
# for each item in the list of values
#  add it to the sum
#  return the sum

def calculate_sum(values):
    if len(values) == 0:
        return None
    sum = [0]
    for it in values:
        sum = sum + it
        return sum
