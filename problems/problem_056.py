# Write a function that meets these requirements.
#
# Name:       num_concat
# Parameters: two numerical parameters
# Returns:    the concatenated string conversions
#             of the numerical parameters
#
# Examples:
#     input:
#       parameters: 3, 10
#     returns: "310"
#     input:
#       parameters: 9238, 0
#     returns: "92380"


def num_concat(num1, num2):
    return str(num1) + str(num2)


print(num_concat(3, 10))
print(num_concat(9238, 0))
