# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    contains_number = False
    contains_special = False
    contains_upper = False
    contains_lower = False
    for char in password:
        if char.isalpha():
            if char.isupper():
                contains_upper = True
            else:
                contains_lower = True
        elif char.isdigit():
            contains_number = True
        elif (char == "$" or char == "!" or char == "@" or char == "#"
              or char == "&" or char == "*" or char == "^" or char == "~"
              or char == "`" or char == "|" or char == "?"
              or char == ":" or char == ";" or char == "," or char == "="
              or char == "(" or char == ")" or char == "{" or char == "}"
              or char == "[" or char == "]"
              ):
            contains_special = True
    return (
        len(password) >= 6
        and len(password) <= 12
        and contains_lower
        and contains_upper
        and contains_number
        and contains_special
    )


print(check_password("password"))
print(check_password("Password"))
print(check_password("PASSWORD"))
print(check_password("passw0rd"))
print(check_password("Passw0rd"))
print(check_password("PASSW0RD"))
print(check_password("P@ssw0rd"))
print(check_password("p@ssw0rd"))
