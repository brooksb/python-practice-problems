# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    maxi = 0
    if len(values) == 0:
        return None
    for v in values:
        if v > maxi:
            maxi = v
    return maxi


print(max_in_list[1, 5, 3, 7, 8])
