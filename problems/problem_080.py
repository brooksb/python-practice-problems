# Write a class that meets these requirements.
#
# Name:       Receipt
#
# Required state:
#    * tax rate, the percentage tax that should be applied to the total
#
# Behavior:
#    * add_item(item)   # Add a ReceiptItem to the Receipt
#    * get_subtotal()   # Returns the total of all of the receipt items
#    * get_total()      # Multiplies the subtotal by the 1 + tax rate
#
# Example:
class RecieptItem:
    def __init__(self, quantity, price) -> None:
        self.quantity = quantity
        self.price = price

    def get_total(self):
        return self.quantity * self.price


class Receipt:
    def __init__(self, tax_rate) -> None:
        self.tax_rate = tax_rate
        self.items = []

    def add_item(self, item):
        self.items.append(item)

    def get_subtotal(self):
        receipt_sum = 0
        for item in self.items:
            receipt_sum += item.get_total()
        return receipt_sum

    def get_total(self):
        return self.get_subtotal() * (1 + self.tax_rate)


item = Receipt(.1)
item.add_item(RecieptItem(4, 2.50))
item.add_item(RecieptItem(2, 5.00))

print(item.get_subtotal())
print(item.get_total())
